import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './form/login/login.component';
import { RegisterComponent } from './form/register/register.component';
import { HeaderComponent } from './header/header.component';
import { JavaComponent } from './Category/java/java.component';
import { PythonComponent } from './Category/python/python.component';
import { WebtechComponent } from './Category/webtech/webtech.component';
import { DatabaseComponent } from './Category/database/database.component';
import { CreateblogComponent } from './createblog/createblog.component';
import { HomeComponent } from './home/home.component';



export const routes: Routes = [
  {path:'',redirectTo:'home',pathMatch:'full'},
  {path:'home',component:HomeComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'header',component:HeaderComponent},
  {path:'createblog',component:CreateblogComponent},
  {path:'java/:id',component:JavaComponent},
  {path:'python/:id',component:PythonComponent},
  {path:'webtech/:id',component:WebtechComponent},
  {path:'database/:id',component:DatabaseComponent},
  //  {path:'**',redirectTo:'home',pathMatch:'full'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
