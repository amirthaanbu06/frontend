import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Register } from '../model/register';
import { HttpClient } from '@angular/common/http';
import { Blog } from '../model/blog';
import { NumberFormatStyle } from '@angular/common';
@Injectable({
  providedIn: 'root'
})
export class SpringConectionService {
  getBlog: any;
  baseURL = 'http://localhost:8080'

  constructor(public httpClient:HttpClient) { }

  private URLRegistration="http://localhost:8080/restController1/register";
  createRegistration(blogRegister:Register):Observable<object>{
   
    return this.httpClient.post(this.URLRegistration,blogRegister);
  }

  private URLLogin="http://localhost:8080/restController1/login";
  loginuser(bloguser:any){
    return this.httpClient.post(this.URLLogin,bloguser);
  }

  private URLCreateBlog="http://localhost:8080/restController1/createblog";
  createBlog(createBlog:any){
    console.log(111111111); 
    return this.httpClient.post(this.URLCreateBlog,createBlog);
  }

  private URLJava="http://localhost:8080/restController1/java";

  getJava(id:Number):Observable<Blog[]>{
    return this.httpClient.get<Blog[]>(this.URLJava+id)
  }
  GetList(id:number){
    return this.httpClient.get(this.baseURL+'/restController1/list/'+id)
  }
}
