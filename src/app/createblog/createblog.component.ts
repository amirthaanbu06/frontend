import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators ,FormBuilder} from '@angular/forms';
import { Router } from '@angular/router';
import { SpringConectionService } from '../serviceFolder/spring-conection.service';
import { Category } from '../model/category';
import { Blog } from '../model/blog';


@Component({
  selector: 'app-createblog',
  templateUrl: './createblog.component.html',
  styleUrls: ['./createblog.component.css']
})
export class CreateblogComponent implements OnInit {
  // firstForm! :FormGroup
  post!:FormGroup

  userCreateBlog :Blog=new Blog();
  addBlog!:String|undefined;
  listArray:any[]=[]
  constructor(private urlService:SpringConectionService,private router: Router,private fb:FormBuilder){

    // this.firstForm=this.fb.group({
    //   blogTitle:['',Validators.required],
    //   blogDescription:['',Validators.required],
    //   catId:['',Validators.required],
    // })
    this.post=this.fb.group({
      blogTitle:new FormControl("",Validators.required),
      blogDescription:new FormControl("",Validators.required),
      catId:new FormControl("",Validators.required)
      // authorName:new FormControl("",Validators.required),
    })
    this.listArray=[
      { name :'Java',id:1},
      { name :'Python',id:2},
      { name :'Webtech',id:3},
      { name :'Database',id:4},
     ]
  }
  ngOnInit(): void {

  }

  navicate(){
    this.getpost();
    this.router.navigateByUrl('/header');
  }
  getId(evt:any){
  const getIdnty = this.listArray.find((ele:any)=> ele.name === evt.target.value);
  if(getIdnty){
    this.post.controls['catId'].setValue(getIdnty.id);
    this.userCreateBlog.catId= getIdnty.name
  }
  }
  getpost(){
    console.log(111111,this.userCreateBlog)
    if(this.post.status === 'VALID'){
      let object:Blog ={
      blogTitle:this.post.controls['blogTitle'].value,
      blogDescription:this.post.controls['blogDescription'].value,
      catId:this.post.controls['catId'].value
      }
      this.urlService.createBlog(object).subscribe((result)=>{
        console.log(result);
        if(result){
          alert("Blog added sucessuly")
        }
      },
      (error)=>{
        this.addBlog="Blog added failed"
      }

    );
    }else{
     alert('form not valid')
    }

  }
}



