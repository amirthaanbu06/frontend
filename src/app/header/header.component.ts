import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent  implements  OnInit{
  listArray:any=[];
  constructor(private router:Router){
    this.listArray=[
      { name :'Java',id:1},
      { name :'Python',id:2},
      { name :'Webtech',id:3},
      { name :'Database',id:4},
     ]
  }

  ngOnInit(): void {


  }
  Navigate(evt:any){
    if (evt === 1) {
      this.router.navigate(['/java/'+evt])
    } else if(evt === 2) {
      this.router.navigate(['/python/'+evt])
    }else if(evt === 3){
      this.router.navigate(['/webtech/'+evt])
    }else if(evt=== 4){
      this.router.navigate(['/database/'+evt])
    }

  }
}
