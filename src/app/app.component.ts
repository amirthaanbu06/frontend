import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'project';


  selectroute: boolean=false;
  constructor(private router: Router)
  {
    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        if(event.url.includes('login') ||event.url==='/' ||event.url==='/register' ){
          this.selectroute = false;
        }
        else{
          this.selectroute = true;
        }
      }
    });
  }
}
