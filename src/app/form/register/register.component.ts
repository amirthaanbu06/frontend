import { Component } from '@angular/core';
import {FormGroup,FormControl, Validators} from '@angular/forms'
import { SpringConectionService } from 'src/app/serviceFolder/spring-conection.service';
import { Register } from 'src/app/model/register';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  title = 'RegisterForm';
  register=new FormGroup({
    name:new FormControl("",Validators.required),
    email:new FormControl("",Validators.email),
    password:new FormControl("",Validators.minLength(6)),
    
  })

  userRegisterationModel:Register=new Register();
  constructor(public urlService:SpringConectionService,private router: Router  ){

  }
  getData(){
    console.log(this.userRegisterationModel)
    this.urlService.createRegistration(this.userRegisterationModel).subscribe((_data)=>{
    });
   
  }
  get vname(){
    return this.register.get("name")
  }
  get vpassword(){
    return this.register.get("password")
  }
  get vemail(){
    return this.register.get("email")
  }
  registervaild(){
    if(this.register.valid){
      this.getData();
      this.router.navigateByUrl('/login');
    }
  }
}
