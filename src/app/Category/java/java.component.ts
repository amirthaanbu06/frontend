import { Component , OnInit} from '@angular/core';
import { Router ,ActivatedRoute} from '@angular/router';
import { Blog } from 'src/app/model/blog';
import { SpringConectionService } from 'src/app/serviceFolder/spring-conection.service';

@Component({
  selector: 'app-java',
  templateUrl: './java.component.html',
  styleUrls: ['./java.component.css']
})
export class JavaComponent implements OnInit{
  createJava:Blog[] = [];



 constructor(private urlService:SpringConectionService,private router: Router,private ActivateRoute:ActivatedRoute){

  this.ActivateRoute.params.subscribe((res:any)=>{
    this.urlService.GetList(res.id).subscribe((res:any)=>{
      this.createJava.push(...res);
      console.log(this.createJava)
    })
  })
 }

  ngOnInit() {

  }
}
