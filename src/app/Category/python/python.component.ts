import { Component, OnInit } from '@angular/core';
import { Blog } from 'src/app/model/blog';
import { SpringConectionService } from 'src/app/serviceFolder/spring-conection.service';
import { Router ,ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-python',
  templateUrl: './python.component.html',
  styleUrls: ['./python.component.css']
})
export class PythonComponent implements OnInit{
  createJava:Blog[] = [];



  constructor(private urlService:SpringConectionService,private router: Router,private ActivateRoute:ActivatedRoute){

   this.ActivateRoute.params.subscribe((res:any)=>{
     this.urlService.GetList(res.id).subscribe((res:any)=>{
       this.createJava.push(...res);
       console.log(this.createJava)
     })
   })
}  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }
}
